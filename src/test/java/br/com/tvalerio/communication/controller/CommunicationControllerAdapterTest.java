package br.com.tvalerio.communication.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import br.com.tvalerio.communication.port.CommunicationRepositoryPort;
import br.com.tvalerio.communication.service.CommunicationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class CommunicationControllerAdapterTest {

  @Autowired
  private CommunicationService service;

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private CommunicationRepositoryPort repository;

  @Test
  @Sql({"/sql/import_communication_api.sql"})
  public void should_find_one_communication() throws Exception {
    mockMvc.perform(get("/communication/{id}", 1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.createdAt", is("2020-08-10T09:00:00")))
        .andExpect(jsonPath("$.sendAt", is("2020-08-20 09:00:00")))
        .andExpect(jsonPath("$.recipient", is("11111111111")))
        .andExpect(jsonPath("$.message", is("Mensagem teste")))
        .andExpect(jsonPath("$.status", is("PENDING")))
        .andExpect(jsonPath("$.type", is("SMS")));
  }

  @Test
  public void should_not_find_inexistent_communication() throws Exception {
    mockMvc.perform(get("/communication/{id}", 1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  public void should_create_communication() throws Exception {
    mockMvc.perform(post("/communication")
        .content("{\"recipient\": \"11111111111\", \"message\":"
            + "\"Mensagem teste\", \"type\": \"email\","
            + "\"sendAt\": \"2020-08-20 09:00:00\"}")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(
            MockMvcResultMatchers.header()
                .string("location", containsString("/communication/1")));
  }

  @Test
  public void should_not_create_communication_with_invalid_request() throws Exception {
    mockMvc.perform(post("/communication")
        .content("{\"recipient\": \"11111111111\", \"message\":"
            + "\"Mensagem teste\", \"type\": \"TIPO_INEXISTENTE\","
            + "\"sendAt\": \"2020-08-20 09:00:00\"}")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$",
            is("Erro ao criar um agendamento de comunicação. Verifique o formato de entrada.")));
  }

  @Test
  @Sql({"/sql/import_communication_api.sql"})
  public void should_delete_communication() throws Exception {
    mockMvc.perform(delete("/communication/{id}", 1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void should_not_delete_inexistent_communication() throws Exception {
    mockMvc.perform(delete("/communication/{id}", 1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

}
