package br.com.tvalerio.communication.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import br.com.tvalerio.communication.domain.Communication;
import br.com.tvalerio.communication.domain.CommunicationStatus;
import br.com.tvalerio.communication.domain.CommunicationType;
import br.com.tvalerio.communication.port.CommunicationRepositoryPort;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class CommunicationServiceAdapterTest {

  @Autowired
  private CommunicationRepositoryPort repository;

  private CommunicationService service;

  @Before
  public void init() {
    service = new CommunicationService(repository);
  }

  @Test
  public void should_save_communication() {
    LocalDateTime sendAtDateTime = LocalDateTime.of(2020, 8, 12, 9, 0, 0);
    String testMessage = "Mensagem teste";
    String testRecipient = "11111111111";

    Communication communication = new Communication();
    communication.setSendAt(sendAtDateTime);
    communication.setMessage(testMessage);
    communication.setRecipient(testRecipient);
    communication.setType(CommunicationType.WHATSAPP);

    Long newCommunicationId = service.create(communication);
    Optional<Communication> newCommunication = service.findOne(newCommunicationId);

    assertThat(newCommunication).isNotNull().isNotEmpty();
    assertThat(newCommunication.get().getId()).isNotNull().isInstanceOf(Long.class);
    assertThat(newCommunication.get()).isNotNull().isInstanceOf(Communication.class);
    assertThat(newCommunication.get().getStatus()).isEqualTo(CommunicationStatus.PENDING);
    assertThat(newCommunication.get().getCreatedAt()).isNotNull().isInstanceOf(LocalDateTime.class);
    assertThat(newCommunication.get().getSendAt()).isNotNull().isInstanceOf(LocalDateTime.class)
        .isEqualTo(sendAtDateTime);
    assertThat(newCommunication.get().getMessage()).isNotNull().isNotEmpty().isEqualTo(testMessage);
    assertThat(newCommunication.get().getRecipient()).isNotNull().isNotEmpty()
        .isEqualTo(testRecipient);
    assertThat(newCommunication.get().getType()).isNotNull().isEqualTo(CommunicationType.WHATSAPP);
  }

  @Test
  @Sql({"/sql/import_communication.sql"})
  public void should_find_one_communication() {
    LocalDateTime createdAtDateTime = LocalDateTime.of(2020, 8, 10, 9, 0, 0);
    LocalDateTime sendAtDateTime = LocalDateTime.of(2020, 8, 20, 9, 0, 0);

    Optional<Communication> communication = service.findOne(1L);

    assertThat(communication).isNotNull().isNotEmpty();
    assertThat(communication.get()).isNotNull().isInstanceOf(Communication.class);
    assertThat(communication.get().getId()).isNotNull().isInstanceOf(Long.class).isEqualTo(1L);
    assertThat(communication.get().getStatus()).isEqualTo(CommunicationStatus.PENDING);
    assertThat(communication.get().getCreatedAt()).isNotNull().isInstanceOf(LocalDateTime.class)
        .isEqualTo(createdAtDateTime);
    assertThat(communication.get().getSendAt()).isNotNull().isInstanceOf(LocalDateTime.class)
        .isEqualTo(sendAtDateTime);
    assertThat(communication.get().getMessage()).isNotNull().isNotEmpty()
        .isEqualTo("Mensagem teste");
    assertThat(communication.get().getRecipient()).isNotNull().isNotEmpty()
        .isEqualTo("11111111111");
    assertThat(communication.get().getType()).isNotNull().isEqualTo(CommunicationType.SMS);
  }

  @Test
  @Sql({"/sql/import_communication.sql"})
  public void should_delete_communication() {
    service.delete(1L);
    Optional<Communication> communication = service.findOne(1L);

    assertThat(communication).isNotNull().isEmpty();
  }

  @Test
  public void should_not_delete_inexistent_communication() {
    assertThatThrownBy(() -> service.delete(1L))
        .isInstanceOf(EmptyResultDataAccessException.class);
  }

  @Test
  @Sql({"/sql/import_communication.sql"})
  public void should_mark_communication_as_sended() {
    LocalDateTime createdAtDateTime = LocalDateTime.of(2020, 8, 10, 9, 0, 0);
    LocalDateTime sendAtDateTime = LocalDateTime.of(2020, 8, 20, 9, 0, 0);

    Communication updateCommunication = service.markAsSended(service.findOne(1L).get());

    assertThat(updateCommunication).isNotNull();
    assertThat(updateCommunication).isNotNull().isInstanceOf(Communication.class);
    assertThat(updateCommunication.getId()).isNotNull().isInstanceOf(Long.class).isEqualTo(1L);
    assertThat(updateCommunication.getStatus()).isEqualTo(CommunicationStatus.SENDED);
    assertThat(updateCommunication.getCreatedAt()).isNotNull().isInstanceOf(LocalDateTime.class)
        .isEqualTo(createdAtDateTime);
    assertThat(updateCommunication.getSendAt()).isNotNull().isInstanceOf(LocalDateTime.class)
        .isEqualTo(sendAtDateTime);
    assertThat(updateCommunication.getMessage()).isNotNull().isNotEmpty()
        .isEqualTo("Mensagem teste");
    assertThat(updateCommunication.getRecipient()).isNotNull().isNotEmpty()
        .isEqualTo("11111111111");
    assertThat(updateCommunication.getType()).isNotNull().isEqualTo(CommunicationType.SMS);
  }

  @Test
  public void should_not_mark_inexistent_communication_as_sended() {
    assertThatThrownBy(() -> service.markAsSended(new Communication()))
        .isInstanceOf(InvalidDataAccessApiUsageException.class);
  }

  @Test
  @Sql({"/sql/import_many_communication.sql"})
  public void should_find_pageable_communications() {
    List<Communication> communicationList = service
        .findPaginatedByTypeAndStatusPendingSortedBySendAt(CommunicationType.SMS, 0, 4);

    assertThat(communicationList).isNotNull().isNotEmpty().hasSize(4);

    assertThat(
        communicationList.stream().filter(c -> c.getStatus().equals(CommunicationStatus.SENDED))
            .collect(
                Collectors.toList())).hasSize(0);

    assertThat(communicationList.get(0).getId()).isEqualTo(7L);
    assertThat(communicationList.get(0).getSendAt()).isEqualTo("2020-08-18T09:00");
    assertThat(communicationList.get(0).getStatus()).isEqualTo(CommunicationStatus.PENDING);
    assertThat(communicationList.get(0).getType()).isEqualTo(CommunicationType.SMS);

    assertThat(communicationList.get(1).getId()).isEqualTo(8L);
    assertThat(communicationList.get(1).getSendAt()).isEqualTo("2020-08-19T09:00");
    assertThat(communicationList.get(1).getStatus()).isEqualTo(CommunicationStatus.PENDING);
    assertThat(communicationList.get(1).getType()).isEqualTo(CommunicationType.SMS);

    assertThat(communicationList.get(2).getId()).isEqualTo(1L);
    assertThat(communicationList.get(2).getSendAt()).isEqualTo("2020-08-20T09:00");
    assertThat(communicationList.get(2).getStatus()).isEqualTo(CommunicationStatus.PENDING);
    assertThat(communicationList.get(2).getType()).isEqualTo(CommunicationType.SMS);

    assertThat(communicationList.get(3).getId()).isEqualTo(2L);
    assertThat(communicationList.get(3).getSendAt()).isEqualTo("2020-08-20T09:00");
    assertThat(communicationList.get(3).getStatus()).isEqualTo(CommunicationStatus.PENDING);
    assertThat(communicationList.get(3).getType()).isEqualTo(CommunicationType.SMS);

  }

}
