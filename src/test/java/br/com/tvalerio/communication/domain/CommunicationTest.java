package br.com.tvalerio.communication.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CommunicationTest {

  @Test
  public void should_create_communication_correctly() {
    Communication communication = new Communication();
    assertThat(communication.getCreatedAt()).isNotNull().isInstanceOf(LocalDateTime.class);
    assertThat(communication.getStatus()).isNotNull().isEqualTo(CommunicationStatus.PENDING);
  }

}
