package br.com.tvalerio.communication.domain;

public enum CommunicationType {
  EMAIL, SMS, PUSH, WHATSAPP;
}
