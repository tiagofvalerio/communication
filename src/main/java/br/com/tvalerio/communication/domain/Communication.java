package br.com.tvalerio.communication.domain;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "communication")
public class Communication {

  @Id
  @GeneratedValue
  @Column(name = "id")
  private Long id;

  @Column(name = "created_at", nullable = false)
  private LocalDateTime createdAt;

  @Column(name = "send_at", nullable = false)
  private LocalDateTime sendAt;

  @Column(name = "recipient", nullable = false)
  private String recipient;

  @Column(name = "message", nullable = false)
  private String message;

  @Column(name = "status", nullable = false)
  @Enumerated(EnumType.STRING)
  private CommunicationStatus status;

  @Column(name = "type", nullable = false)
  @Enumerated(EnumType.STRING)
  private CommunicationType type;

  public Communication() {
    this.setStatus(CommunicationStatus.PENDING);
    this.createdAt = LocalDateTime.now();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public String getRecipient() {
    return recipient;
  }

  public void setRecipient(String recipient) {
    this.recipient = recipient;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public CommunicationStatus getStatus() {
    return status;
  }

  public void setStatus(CommunicationStatus status) {
    this.status = status;
  }

  public CommunicationType getType() {
    return type;
  }

  public void setType(CommunicationType type) {
    this.type = type;
  }

  public LocalDateTime getSendAt() {
    return sendAt;
  }

  public void setSendAt(LocalDateTime sendAt) {
    this.sendAt = sendAt;
  }

  public Communication markAsSended() {
    this.setStatus(CommunicationStatus.SENDED);
    return this;
  }
}
