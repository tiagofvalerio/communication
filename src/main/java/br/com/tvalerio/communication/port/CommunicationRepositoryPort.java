package br.com.tvalerio.communication.port;

import br.com.tvalerio.communication.domain.Communication;
import br.com.tvalerio.communication.domain.CommunicationType;
import java.util.List;
import java.util.Optional;
import org.springframework.dao.InvalidDataAccessApiUsageException;

public interface CommunicationRepositoryPort {

  Communication create(Communication communication);

  Optional<Communication> findOne(Long id);

  void delete(Long id);

  Communication markAsSended(Communication communication) throws InvalidDataAccessApiUsageException;

  List<Communication> findPaginatedByTypeAndStatusPendingSortedBySendAt(CommunicationType type,
      Integer pageNumber,
      Integer pageSize);

}
