package br.com.tvalerio.communication.port;

import br.com.tvalerio.communication.adapter.dto.CommunicationDTO;
import br.com.tvalerio.communication.domain.Communication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface CommunicationApiPort {

  @PostMapping
  public ResponseEntity create(@RequestBody CommunicationDTO communicationDTO);

  @GetMapping("/{id}")
  public ResponseEntity getCommunication(@PathVariable Long id);

  @DeleteMapping("/{id}")
  public ResponseEntity delete(@PathVariable Long id);

}
