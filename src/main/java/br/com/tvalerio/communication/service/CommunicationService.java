package br.com.tvalerio.communication.service;

import br.com.tvalerio.communication.domain.Communication;
import br.com.tvalerio.communication.domain.CommunicationType;
import br.com.tvalerio.communication.port.CommunicationRepositoryPort;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Component;

@Component
public class CommunicationService {

  private CommunicationRepositoryPort communicationRepositoryPort;

  public CommunicationService(
      CommunicationRepositoryPort communicationRepositoryPort) {
    this.communicationRepositoryPort = communicationRepositoryPort;
  }

  public Long create(Communication communication) {
    return communicationRepositoryPort.create(communication).getId();
  }

  public Optional<Communication> findOne(Long id) {
    return communicationRepositoryPort.findOne(id);
  }

  public void delete(Long id) {
    this.communicationRepositoryPort.delete(id);
  }

  public Communication markAsSended(Communication communication) {
    return this.communicationRepositoryPort.markAsSended(communication);
  }

  public List<Communication> findPaginatedByTypeAndStatusPendingSortedBySendAt(
      CommunicationType type,
      Integer pageNumber,
      Integer pageSize) {
    return this.communicationRepositoryPort
        .findPaginatedByTypeAndStatusPendingSortedBySendAt(type, pageNumber, pageSize);
  }

}
