package br.com.tvalerio.communication.adapter.repository;

import br.com.tvalerio.communication.domain.Communication;
import br.com.tvalerio.communication.domain.CommunicationStatus;
import br.com.tvalerio.communication.domain.CommunicationType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CommunicationRepository extends PagingAndSortingRepository<Communication, Long> {

  Page<Communication> findByTypeAndStatus(CommunicationType type, CommunicationStatus status,
      Pageable pageable);

}
