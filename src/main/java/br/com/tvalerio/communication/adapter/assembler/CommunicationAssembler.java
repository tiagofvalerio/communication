package br.com.tvalerio.communication.adapter.assembler;

import br.com.tvalerio.communication.adapter.dto.CommunicationDTO;
import br.com.tvalerio.communication.domain.Communication;
import br.com.tvalerio.communication.domain.CommunicationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommunicationAssembler {

  private static final Logger LOG = LoggerFactory.getLogger(CommunicationAssembler.class);

  public static Communication convertToCommunicationDomain(CommunicationDTO dto)
      throws InvalidDataException {
    Communication communication = new Communication();

    try {
      communication.setRecipient(dto.getRecipient());
      communication.setMessage(dto.getMessage());
      communication.setSendAt(dto.getSendAt());

      communication.setType(CommunicationType.valueOf(dto.getType().toUpperCase()));
    } catch (Exception e) {
      LOG.debug("Error: ", e);
      throw new InvalidDataException("Erro ao converter JSON de entrada.", e);
    }

    return communication;
  }

  public static CommunicationDTO convertToCommunicationDTO(Communication communication) {
    CommunicationDTO dto = new CommunicationDTO();

    dto.setCreatedAt(communication.getCreatedAt());
    dto.setRecipient(communication.getRecipient());
    dto.setMessage(communication.getMessage());
    dto.setStatus(communication.getStatus().toString());
    dto.setType(communication.getType().toString());
    dto.setSendAt(communication.getSendAt());

    return dto;
  }
}
