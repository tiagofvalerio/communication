package br.com.tvalerio.communication.adapter.assembler;

public class InvalidDataException extends Exception {

  public InvalidDataException(String message, Throwable cause) {
    super(message, cause);
  }
}
