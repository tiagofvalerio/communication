package br.com.tvalerio.communication.adapter.controller;

import br.com.tvalerio.communication.adapter.assembler.CommunicationAssembler;
import br.com.tvalerio.communication.adapter.dto.CommunicationDTO;
import br.com.tvalerio.communication.domain.Communication;
import br.com.tvalerio.communication.port.CommunicationApiPort;
import br.com.tvalerio.communication.service.CommunicationService;
import java.net.URI;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/communication")
public class CommunicationControllerAdapter implements CommunicationApiPort {

  private static final Logger LOG = LoggerFactory.getLogger(CommunicationControllerAdapter.class);

  private CommunicationService communicationService;

  @Autowired
  public CommunicationControllerAdapter(
      CommunicationService communicationService) {
    this.communicationService = communicationService;
  }

  @Override
  public ResponseEntity create(@RequestBody CommunicationDTO communicationDTO) {
    try {
      final Long id = this.communicationService
          .create(CommunicationAssembler.convertToCommunicationDomain(communicationDTO));

      URI location = ServletUriComponentsBuilder
          .fromCurrentRequest()
          .path("/{id}")
          .buildAndExpand(id)
          .toUri();

      return ResponseEntity.created(location).build();
    } catch (Exception e) {
      LOG.debug("Error: ", e);
      LOG.error(e.getMessage());
      return ResponseEntity.badRequest()
          .body("Erro ao criar um agendamento de comunicação. Verifique o formato de entrada.");
    }
  }

  @Override
  public ResponseEntity getCommunication(@PathVariable Long id) {
    Optional<Communication> communication = this.communicationService.findOne(id);

    if (communication.isPresent()) {
      return ResponseEntity
          .ok(CommunicationAssembler.convertToCommunicationDTO(communication.get()));
    } else {
      return ResponseEntity.notFound().build();
    }
  }

  @Override
  public ResponseEntity delete(@PathVariable Long id) {
    try {
      this.communicationService.delete(id);
      return ResponseEntity.ok().build();
    } catch (EmptyResultDataAccessException e) {
      LOG.debug("Error: ", e);
      LOG.error(e.getMessage());
      return ResponseEntity.notFound().build();
    }
  }
}
