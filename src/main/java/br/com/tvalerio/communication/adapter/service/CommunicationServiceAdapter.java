package br.com.tvalerio.communication.adapter.service;

import br.com.tvalerio.communication.adapter.repository.CommunicationRepository;
import br.com.tvalerio.communication.domain.Communication;
import br.com.tvalerio.communication.domain.CommunicationStatus;
import br.com.tvalerio.communication.domain.CommunicationType;
import br.com.tvalerio.communication.port.CommunicationRepositoryPort;
import java.util.List;
import java.util.Optional;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class CommunicationServiceAdapter implements CommunicationRepositoryPort {

  private static final String SEND_AT_KEY = "sendAt";

  private CommunicationRepository repository;

  public CommunicationServiceAdapter(
      CommunicationRepository repository) {
    this.repository = repository;
  }

  @Transactional
  @Override
  public Communication create(Communication communication) {
    return repository.save(communication);
  }

  @Override
  public void delete(Long id) {
    repository.deleteById(id);
  }

  @Override
  public Optional<Communication> findOne(Long id) {
    return repository.findById(id);
  }

  @Override
  public Communication markAsSended(Communication communication) {
    if (communication.getId() != null && repository.existsById(communication.getId())) {
      return repository.save(communication.markAsSended());
    } else {
      throw new InvalidDataAccessApiUsageException(
          "Objeto inválido para essa operação. Id está nulo ou não existe na base.");
    }
  }

  @Override
  public List<Communication> findPaginatedByTypeAndStatusPendingSortedBySendAt(
      CommunicationType type,
      Integer pageNumber,
      Integer pageSize) {
    Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(SEND_AT_KEY).ascending());
    Page<Communication> pagedFind = repository
        .findByTypeAndStatus(type, CommunicationStatus.PENDING,
            pageable);
    return pagedFind.toList();
  }
}
