# Communication - Plataforma para agendamento de comunicações.


### Instalação
<br/>

 - Para executar o projeto deve existir um banco de dados Postgres com configurações padrão, com usuário e senha da mesma forma que a definida nos arquivos `application.properties` e `application-test.properties` (Usuário `postgres` e senha em branco). Caso seja necessário uma configuração diferente, deve-se alterar nesses arquivos.
 
 - Devem existir duas bases de dados criadas: `communication` e `communication-test`.
 
 - Para fins de teste, o projeto está configurado para criar o modelo de dados toda vez que é executado.
 
 - O projeto utiliza Java 8 e Maven para executar.

<br/>
Para compilar o projeto e rodar os testes:

`$ mvn clean install` 
<br/>
<br/>
<br/>
Para compilar sem rodar os testes: 
<br/>

`$ mvn clean install -Dmaven.test.skip=true`

<br/>
<br/>

Após compilar e rodar os testes, pode-se ver o relatório de cobertura de testes em:

`target/site/jacoco/index.html`

<br/>
<br/>

Para executar o projeto:

`$ mvn spring-boot:run`

<br/><br/>
### API
<br/>

A documentação da API é gerada utilizando o Swagger e pode ser acessada no endereço abaixo com o projeto em execução: 

`http://localhost:8080/swagger-ui.html` 

<br/><br/>
### Exemplos
 - **Utilizando curl**
<br/>


Para criar um agendamento:

```$ curl -X POST -H 'Content-Type: application/json' -i http://localhost:8080/communication --data '{"message": "Mensagem teste", "recipient": "11111111111", "sendAt": "2020-08-11 06:14:00", "type": "SMS"}'```

<br/><br/>

Para consultar o status (ou consultar um agendamento criado), utilizar o id retornado no location da requisição de criação conforme abaixo:

`$ curl -X GET -H 'Content-Type: application/json' -i http://localhost:8080/communication/1`

<br/><br/>

Para deletar um agendamento, utilizar o id retornado no location da requisição de criação conforme abaixo:

`$ curl -X DELETE -H 'Content-Type: application/json' -i http://localhost:8080/communication/1`